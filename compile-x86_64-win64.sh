#! /usr/bin/bash

build="win64"
binary="$build/bin"
assets="$build/assets"
assetsSource="assets"
output="$binary/TicTacToe.exe"
source="src/*.cpp"

compiler="x86_64-w64-mingw32-g++"
options="-static-libstdc++"

if !([ -d $binary ]); then
  mkdir -p $binary
fi

if !([ -d $assets ]); then
  mkdir -p $assets
fi

include="-Isrc/*.hpp"
include="$include -I lib/SDL2-2.0.14/x86_64-w64-mingw32/include"
include="$include -I lib/SDL2_image-2.0.5/x86_64-w64-mingw32/include"
include="$include -I lib/SDL2_ttf-2.0.15/x86_64-w64-mingw32/include"

libraries="-L lib/SDL2-2.0.14/x86_64-w64-mingw32/lib"
libraries="$libraries -L lib/SDL2_image-2.0.5/x86_64-w64-mingw32/lib"
libraries="$libraries -L lib/SDL2_ttf-2.0.15/x86_64-w64-mingw32/lib"

linking="-lmingw32 -lSDL2main -lSDL2 -lSDL2_image -lSDL2_ttf"

$compiler $source -o$output $options $include $libraries $linking

echo
echo
echo "Finished compiling. Compiled binary is placed at $output."
echo

# Copy Windows Dynamic Linked Libraries (.dll)
dll="lib/SDL2-2.0.14/x86_64-w64-mingw32/bin/*.dll"
dll="$dll lib/SDL2_ttf-2.0.15/x86_64-w64-mingw32/bin/*.dll"
dll="$dll lib/SDL2_image-2.0.5/x86_64-w64-mingw32/bin/*.dll"
dll="$dll lib/mingw32/*.dll"

cp $dll --target-directory=$binary

# Copy assets
cp -r $assetsSource/* $assets
