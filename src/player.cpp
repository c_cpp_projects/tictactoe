#include "player.hpp"

Player::Player(std::string name, SDL_Color color)
    : _name(name), _color(color), _texture(nullptr), _matrix(nullptr) {}

Player::Player(std::string name, Texture* texture)
    : _name(name), _texture(texture), _color({255, 255, 255}), _matrix(nullptr) {}

Player::~Player() {
  if (nullptr != _matrix) free(_matrix);
}

void Player::setWinningCriteria(int criteria) {
  if (criteria <= _boardSize) {
    _winningCriteria = criteria;
  } else {
    printf("ERROR!\nWinningCriteria can not be bigger than the Boards size!\n\n");
  }
}

void Player::setColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a) { _color = {r, g, b, a}; }

SDL_Color Player::getColor() const { return _color; }

std::string Player::getName() const { return _name; }

bool Player::hasWon() const {
  int won;
  const int delta = _boardSize - _winningCriteria;

  // Check all rows
  for (int x = 0; x < _boardSize; x++) {
    for (int i = 0; i <= delta; i++) {
      won = 0;
      for (int y = i; y < _winningCriteria + i; y++) won += getMatrixValueAt(x, y);
      if (won >= _winningCriteria) return true;
    }
  }

  // Check all columns
  for (int y = 0; y < _boardSize; y++) {
    for (int i = 0; i <= delta; i++) {
      won = 0;
      for (int x = i; x < _winningCriteria + i; x++) won += getMatrixValueAt(x, y);
      if (won >= _winningCriteria) return true;
    }
  }

  /**************************************************************/
  // 1 DigonalenArt
  // x-1,y+1

  // teste cols auf diagonalen
  for (int i = 0; i < _boardSize; i++) {
    int x = i;
    int y = 0;
    won = 0;

    for (; x >= 0 && y < _boardSize; x--, y++) {
      won += getMatrixValueAt(x, y);
      if (won >= _winningCriteria) return true;
    }
  }

  // teste rows auf diagonalen (x=cols-1)
  for (int i = 0; i < _boardSize; i++) {
    int x = _boardSize - 1;
    int y = i;
    won = 0;

    for (; x >= 0 && y < _boardSize; x--, y++) {
      won += getMatrixValueAt(x, y);
      if (won >= _winningCriteria) return true;
    }
  }

  // 2 DigonalenArt
  // x+1,y+1

  // teste cols auf diagonalen
  for (int i = 0; i < _boardSize; i++) {
    int x = i;
    int y = 0;
    won = 0;

    for (; x < _boardSize && y < _boardSize; x++, y++) {
      won += getMatrixValueAt(x, y);
      if (won >= _winningCriteria) return true;
    }
  }

  // teste rows auf diagonalen (x=0)
  for (int i = 0; i < _boardSize; i++) {
    int x = 0;
    int y = i;
    won = 0;

    for (; x < _boardSize && y < _boardSize; x++, y++) {
      won += getMatrixValueAt(x, y);
      if (won >= _winningCriteria) return true;
    }
  }

  /**************************************************************/

  return false;
}

bool Player::hasTexture() const { return nullptr != _texture; }

void Player::setBoardSize(int boardSize) {
  _boardSize = boardSize;
  _matrix = (TTileValue*) calloc(getMatrixSize(), sizeof(TTileValue));
  for (int i = 0; i < getMatrixSize(); i++) _matrix[i] = TILE_EMPTY;
}

int Player::getMatrixSize() const { return _boardSize * _boardSize; }

int Player::getMatrixIndex(int x, int y) const {
  int index = y + _boardSize * x;

  if (index >= _boardSize * _boardSize) {
    printf("ERROR in Player, index out of bounds: %d\n", index);
    return -1;
  }

  return index;
}

TTileValue Player::getMatrixValueAt(int x, int y) const {
  return _matrix[getMatrixIndex(x, y)];
}

TTileValue Player::getMatrixValueAt(int index) const { return _matrix[index]; }

void Player::setMatrixValueAt(int x, int y, TTileValue tileValue) {
  _matrix[getMatrixIndex(x, y)] = tileValue;
}

void Player::setMatrixValueAt(TPlayerMove move, TTileValue tileValue) {
  _matrix[getMatrixIndex(move.x, move.y)] = tileValue;
}

void Player::clearMatrix() { std::fill_n(_matrix, getMatrixSize(), TILE_EMPTY); }

void Player::render(SDL_Renderer* renderer, const int x, const int y, const int width,
                    const int height) const {
  SDL_Rect clip = {0, 0, width, height};
  _texture->render(renderer, x, y, &clip);
}
