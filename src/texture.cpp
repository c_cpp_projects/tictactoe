#include "texture.hpp"

Texture::Texture() : _texture(nullptr), _width(0), _height(0) {}

Texture::~Texture() {
  // Deallocate
  free();
}

bool Texture::loadFromFile(SDL_Renderer* renderer, const std::string path) {
  // Get rid of preexisting _texture
  free();

  // Load image at specified path
  SDL_Surface* loadedSurface = IMG_Load(path.c_str());
  if (loadedSurface == nullptr) {
    printf("Unable to load image %s! SDL_image Error: %s\n", path.c_str(),
           IMG_GetError());
    return false;
  }

  // Color key image
  SDL_SetColorKey(loadedSurface, SDL_TRUE,
                  SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));

  _texture = SDL_CreateTextureFromSurface(renderer, loadedSurface);
  if (_texture == nullptr) {
    printf("Unable to create _texture from %s! SDL Error: %s\n", path.c_str(),
           SDL_GetError());
    return false;
  }

  // Get image dimensions
  _width = loadedSurface->w;
  _height = loadedSurface->h;

  // Get rid of old loaded surface
  SDL_FreeSurface(loadedSurface);

  return true;
}

#if defined(SDL_TTF_MAJOR_VERSION)
bool Texture::loadFromRenderedText(SDL_Renderer* renderer, TTF_Font* font,
                                   const std::string textureText,
                                   const SDL_Color textColor) {
  // Get rid of preexisting _texture
  free();

  _textureText = textureText;

  // Render text surface
  SDL_Surface* textSurface = TTF_RenderText_Solid(font, textureText.c_str(), textColor);
  if (nullptr == textSurface) {
    printf("Unable to render text surface! SDL_ttf Error: %s\n", TTF_GetError());
    return false;
  }

  // Create _texture from surface pixels
  _texture = SDL_CreateTextureFromSurface(renderer, textSurface);
  if (nullptr == _texture) {
    printf("Unable to create _texture from rendered text! SDL Error: %s\n",
           SDL_GetError());
    return false;
  }

  // Get image dimension
  _width = textSurface->w;
  _height = textSurface->h;

  // Get rid of old surface
  SDL_FreeSurface(textSurface);

  return true;
}
#endif

void Texture::free() {
  // Free _texture if it exists
  if (_texture != nullptr) {
    SDL_DestroyTexture(_texture);
    _texture = nullptr;
    _width = 0;
    _height = 0;
  }
}

void Texture::setColor(const Uint8 red, const Uint8 green, const Uint8 blue) const {
  // Modulate _texture
  SDL_SetTextureColorMod(_texture, red, green, blue);
}

void Texture::setBlendMode(const SDL_BlendMode blending) const {
  SDL_SetTextureBlendMode(_texture, blending);
}

void Texture::setAlpha(const Uint8 alpha) const {
  SDL_SetTextureAlphaMod(_texture, alpha);
}

void Texture::render(SDL_Renderer* renderer, const int x, const int y,
                     const SDL_Rect* clip, const double angle, const SDL_Point* center,
                     const SDL_RendererFlip flip) const {
  // Set rendering space and render to screen
  SDL_Rect renderQuad = {x, y, _width, _height};

  // Set clip rendering dimensions
  if (nullptr != clip) {
    renderQuad.w = clip->w;
    renderQuad.h = clip->h;
  }

  SDL_RenderCopyEx(renderer, _texture, clip, &renderQuad, angle, center, flip);
}

int Texture::getWidth() const { return _width; }

int Texture::getHeight() const { return _height; }

std::string Texture::getText() const { return _textureText; }