#ifndef PLAYER_HPP
#define PLAYER_HPP

#include <SDL.h>

#include <array>
#include <iostream>

#include "texture.hpp"

typedef enum {
  TILE_EMPTY = 0,
  TILE_OCCUPIED = 1,
} TTileValue;

typedef struct PlayerMove TPlayerMove;

class Player {
  friend class Board;

 private:
  SDL_Color _color;
  TTileValue* _matrix;
  int _boardSize;
  std::string _name;
  Texture* _texture;
  int _winningCriteria;

  void setBoardSize(int boardSize);

  void setWinningCriteria(int criteria);

  int getMatrixSize() const;

  int getMatrixIndex(int x, int y) const;

  TTileValue getMatrixValueAt(int x, int y) const;
  TTileValue getMatrixValueAt(int index) const;

  void setMatrixValueAt(int x, int y, TTileValue tileValue);
  void setMatrixValueAt(TPlayerMove move, TTileValue tileValue);

  void clearMatrix();

  void render(SDL_Renderer* renderer, const int x, const int y, const int width,
              const int height) const;

 public:
  Player(std::string name, SDL_Color color = {255, 255, 255, 255});

  Player(std::string name, Texture* texture = nullptr);

  virtual ~Player();

  void setColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a = 255);
  SDL_Color getColor() const;

  std::string getName() const;

  bool hasWon() const;

  bool hasTexture() const;
};

struct PlayerMove {
  int x;
  int y;
  Player* player;
};

#endif  // PLAYER_HPP