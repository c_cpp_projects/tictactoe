#include "board.hpp"

Board::Board(SDL_Renderer* renderer, std::string font, Player* player1, Player* player2,
             int size, int tiles, int winningCriteria, SDL_Color backgroundColor,
             SDL_Color gridColor, boardEvent::PlayerWon onPlayerWon,
             boardEvent::GameOver onGameOver)
    : _player1(player1),
      _player2(player2),
      _current(player1),
      _outline({0, 0, size, size}),
      _tiles(tiles),
      _backgroundColor(backgroundColor),
      _gridColor(gridColor),
      _onPlayerWon(onPlayerWon),
      _onGameOver(onGameOver) {
  if (_tiles >= winningCriteria) {
    if (nullptr != _player1) {
      _player1->setBoardSize(_tiles);
      _player1->setWinningCriteria(winningCriteria);
    }

    if (nullptr != _player2) {
      _player2->setBoardSize(_tiles);
      _player2->setWinningCriteria(winningCriteria);
    }

    _lastMovesLength = 0;
    _lastMoves = (TPlayerMove*) calloc(_tiles, sizeof(TPlayerMove));

    _textFont = TTF_OpenFont(font.c_str(), getPlayerTileSize() * 0.525);
    _tileIds = (Texture**) calloc(_tiles * _tiles, sizeof(Texture));
    if (nullptr != _tileIds && nullptr != _textFont) {
      char letter;
      char num1;
      char num2;
      std::string id;

      for (int x = 0, letter = 'A'; x < _tiles; x++, letter++) {
        for (int y = 0, num1 = '1', num2 = '0'; y < _tiles; y++) {
          id += letter;
          if ('0' < num2) id += num2;
          id += num1;

          _tileIds[x + _tiles * y] = new Texture();
          _tileIds[x + _tiles * y]->loadFromRenderedText(renderer, _textFont, id.c_str(),
                                                         _gridColor);
          if ('9' > num1) {
            num1++;
          } else {
            num1 = '0';
            num2++;
          }

          id.clear();
        }
      }
    }
  }
}

Board::~Board() {
  for (int i = 0; i < _tiles * _tiles; i++) delete _tileIds[i];
  if (nullptr == _tileIds) free(_tileIds);
  if (nullptr != _textFont) TTF_CloseFont(_textFont);
  if (nullptr != _lastMoves) free(_lastMoves);
}

bool Board::isGameOver() const {
  if (_player1->hasWon()) {
    if (nullptr != _onGameOver) _onGameOver(_player1);
    return true;
  }

  if (_player2->hasWon()) {
    if (nullptr != _onGameOver) _onGameOver(_player2);
    return true;
  }

  // Check if board is filled
  char boardMatrix[_player1->getMatrixSize()];
  bool filled = true;
  for (int i = 0; i < _player1->getMatrixSize(); i++) {
    boardMatrix[i] = _player1->getMatrixValueAt(i);
    boardMatrix[i] += _player2->getMatrixValueAt(i);
  }
  for (int i = 0; i < _player1->getMatrixSize(); i++)
    if (TILE_EMPTY == boardMatrix[i]) filled = false;

  if (filled && nullptr != _onGameOver) _onGameOver(nullptr);

  return filled;
}

Board& Board::setBackgroundColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a) {
  _backgroundColor = {r, g, b, a};
  return *this;
}

Board& Board::setGridColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a) {
  _gridColor = {r, g, b, a};
  return *this;
}

Board& Board::setOnGameOver(const boardEvent::GameOver onGameOver) {
  _onGameOver = onGameOver;
  return *this;
}

Board& Board::setOnPlayerWon(const boardEvent::PlayerWon onPlayerWon) {
  _onPlayerWon = onPlayerWon;
  return *this;
}

void Board::handleEvent(const SDL_Event* e) {
  if (SDL_MOUSEBUTTONDOWN == e->type && !isGameOver()) {
    int y = e->motion.x / getPlayerTileSize();
    int x = e->motion.y / getPlayerTileSize();

    if (TILE_EMPTY == _player1->getMatrixValueAt(x, y) &&
        TILE_EMPTY == _player2->getMatrixValueAt(x, y)) {
      _current->setMatrixValueAt(x, y, TILE_OCCUPIED);

      _lastMovesLength++;
      _lastMoves[_lastMovesLength] = {x, y, _current};
    }

    if (_current->hasWon())
      if (nullptr != _onPlayerWon) _onPlayerWon(_current);

    swapPlayer();
  }
}

void Board::render(SDL_Renderer* renderer, int posX, int posY) {
  const int playerTileSize = getPlayerTileSize();
  _outline.x = posX;
  _outline.y = posY;
  Texture* tileId;

  // Draw board background
  SDL_SetRenderDrawColor(renderer, _backgroundColor.r, _backgroundColor.g,
                         _backgroundColor.b, _backgroundColor.a);
  SDL_RenderFillRect(renderer, &_outline);

  // Render Chessboard like numbering
  for (int x = 0; x < _tiles; x++) {
    for (int y = 0; y < _tiles; y++) {
      if (TILE_EMPTY ==
          _player1->getMatrixValueAt(x, y) + _player2->getMatrixValueAt(x, y)) {
        tileId = _tileIds[y + _tiles * x];
        tileId->render(renderer,
                       y * playerTileSize + ((playerTileSize - tileId->getWidth()) / 2),
                       x * playerTileSize + ((playerTileSize - tileId->getHeight()) / 2));
      }
    }
  }

  // Render player1 tiles
  renderPlayer(renderer, _player1, playerTileSize);

  // Render player2 tiles
  renderPlayer(renderer, _player2, playerTileSize);

  // Draw board grid
  SDL_SetRenderDrawColor(renderer, _gridColor.r, _gridColor.g, _gridColor.b,
                         _gridColor.a);

  for (int i = 1; i < _tiles; i++) {
    // Draw vertical lines
    SDL_RenderDrawLine(renderer, playerTileSize * i, 0, playerTileSize * i, _outline.h);
    // Draw horizontal lines
    SDL_RenderDrawLine(renderer, 0, playerTileSize * i, _outline.w, playerTileSize * i);
  }
}

int Board::getPlayerTileSize() const {
  return (int) ceilf((float) _outline.w / (float) _tiles);
}

void Board::swapPlayer() { _current = (_current == _player1) ? _player2 : _player1; }

void Board::renderPlayer(SDL_Renderer* renderer, const Player* player,
                         const int playerSize) const {
  int x;
  int y;
  SDL_Rect playerPos;

  if (player->hasTexture()) {
    for (y = 0; y < _tiles; y++) {
      for (x = 0; x < _tiles; x++) {
        if (TILE_OCCUPIED == player->getMatrixValueAt(x, y))
          player->render(renderer, _outline.x + playerSize * y,
                         _outline.y + playerSize * x, playerSize, playerSize);
      }
    }
  } else {
    // If player has no texture, then render
    // a colored rectangle
    SDL_SetRenderDrawColor(renderer, player->getColor().r, player->getColor().g,
                           player->getColor().b, player->getColor().a);
    for (y = 0; y < _tiles; y++) {
      for (x = 0; x < _tiles; x++) {
        playerPos = {_outline.x + playerSize * y, _outline.y + playerSize * x, playerSize,
                     playerSize};
        if (TILE_OCCUPIED == player->getMatrixValueAt(x, y))
          SDL_RenderFillRect(renderer, &playerPos);
      }
    }
  }
}

void Board::reset() {
  _player1->clearMatrix();
  _player2->clearMatrix();
  _current = _player1;
}

void Board::undoLastMove() {
  if (0 < _lastMovesLength) {
    _lastMoves[_lastMovesLength].player->setMatrixValueAt(
        _lastMoves[_lastMovesLength].x, _lastMoves[_lastMovesLength].y, TILE_EMPTY);
    _lastMovesLength--;
  }
}
