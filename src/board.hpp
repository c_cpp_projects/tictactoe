#ifndef BOARD_HPP
#define BOARD_HPP

#include <SDL.h>
#include <SDL_ttf.h>
#include <math.h>

#include "player.hpp"

namespace boardEvent {
typedef void (*GameOver)(const Player* winner);

typedef void (*PlayerWon)(const Player* winner);
}  // namespace boardEvent

class Board {
 private:
  SDL_Rect _outline;
  int _tiles;
  Texture** _tileIds;
  TTF_Font* _textFont;

  SDL_Color _backgroundColor;
  SDL_Color _gridColor;

  Player* _player1;
  Player* _player2;
  Player* _current;

  TPlayerMove* _lastMoves;
  size_t _lastMovesLength;

  boardEvent::PlayerWon _onPlayerWon;
  boardEvent::GameOver _onGameOver;

  int getPlayerTileSize() const;
  void swapPlayer();
  void renderPlayer(SDL_Renderer* renderer, const Player* player,
                    const int playerSize) const;

 public:
  Board(SDL_Renderer* renderer, std::string font, Player* player1, Player* player2,
        int size, int tiles = 3, int winningCriteria = 3,
        SDL_Color backgroundColor = {255, 255, 255, 255},
        SDL_Color gridColor = {0, 0, 0, 255}, boardEvent::PlayerWon onPlayerWon = nullptr,
        boardEvent::GameOver onGameOver = nullptr);

  virtual ~Board();

  Board& setBackgroundColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a = 255);

  Board& setGridColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a = 255);

  Board& setOnGameOver(const boardEvent::GameOver onGameOver);

  Board& setOnPlayerWon(const boardEvent::PlayerWon onPlayerWon);

  bool isGameOver() const;

  void handleEvent(const SDL_Event* e);

  void render(SDL_Renderer* renderer, int posX = 0, int posY = 0);

  void reset();

  void undoLastMove();
};

#endif  // BOARD_HPP