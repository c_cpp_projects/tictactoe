#include <SDL.h>
#include <SDL_ttf.h>

#include <iostream>
#include <sstream>

#include "board.hpp"
#include "player.hpp"
#include "texture.hpp"
#include "timer.hpp"

#define ASSETS            "../assets"
#define FONTS(font)       (ASSETS "/fonts/" font)
#define TEXTURES(texture) (ASSETS "/textures/" texture)

#define APPLICATION_NAME       ("TicTacToe")
#define SCREEN_SIZE            (900)
#define SCREEN_FPS             (100)
#define SCREEN_TICKS_PER_FRAME (1000 / SCREEN_FPS)
#if 0
#define SHOW_FPS
#endif

#define FONT_SIZE (50)

#define BOARD_SIZE       (12)
#define WINNING_CRITERIA (5)

SDL_Window* gWindow;
SDL_Renderer* gRenderer;
SDL_Event gEvent;

TTF_Font* gFont;
Texture gTextTexture;
Texture gReplayTexture;
Texture gQuitTexture;
Texture gTextTexture3;
Texture crossTexture;
Texture circleTexture;
Texture gFPSTexture;
Texture gEndScreen;

const SDL_Color textColor = {0, 0, 0, 255};
const SDL_Color fpsTextColor = {0, 255, 0, 255};

// The fps cap timer
Timer capTimer;
unsigned int frameTicks;
Uint32 countedFrames;
float avgFPS;
// The application timer
Timer fpsTimer;
// In memory text stream
std::stringstream fpsText;

Board* gameBoard;

bool quit;

void onGameOver(const Player* winner);

bool init();
void close();
bool loadMedia();

int main(int argc, char** argv) {
  if (!init()) return 1;
  if (!loadMedia()) return 2;

  // Main loop flag
  quit = false;

  Player a = Player("ObeE", &crossTexture);
  Player b = Player("SMIJECK", &circleTexture);
  gameBoard = new Board(gRenderer, FONTS("DotGothic16-Regular.ttf"), &a, &b, SCREEN_SIZE,
                        BOARD_SIZE, WINNING_CRITERIA);

  gameBoard->setBackgroundColor(255, 255, 255).setOnGameOver(&onGameOver);

  // Start counting fps
  countedFrames = 0;
  fpsTimer.start();
  avgFPS = 0;

  while (1) {
    // Start cap time
    capTimer.start();

    if (0 != SDL_PollEvent(&gEvent)) {
      if (SDL_QUIT == gEvent.type) {
        quit = true;
      }

      if (SDL_KEYDOWN == gEvent.type) {
        switch (gEvent.key.keysym.sym) {
          case SDLK_r:
            quit = false;
            gameBoard->reset();
            break;

          case SDLK_u:
            gameBoard->undoLastMove();
            break;

          default:
            break;
        }
      }

      gameBoard->handleEvent(&gEvent);
    }

    if (quit) break;

    // Calculate and correct fps
    avgFPS = countedFrames / (fpsTimer.getTicks() / 1000.f);
    if (avgFPS > 2000000) avgFPS = 0;

#ifdef SHOW_FPS
    fpsText.str("");
    fpsText << (int) avgFPS;

    // Load fps text texture
    if (!gFPSTexture.loadFromRenderedText(gRenderer, gFont, fpsText.str().c_str(),
                                          fpsTextColor)) {
      printf("Unable to render render time text! Error: %s\n", TTF_GetError());
    }
#endif

    if (!gameBoard->isGameOver()) {
      // Initialize renderer color
      SDL_SetRenderDrawColor(gRenderer, 255, 255, 255, 255);

      // Clear screen
      SDL_RenderClear(gRenderer);

      gameBoard->render(gRenderer);
#ifdef SHOW_FPS
      gFPSTexture.render(gRenderer, 0, 0);
#endif

      // Update screen
      SDL_RenderPresent(gRenderer);
    }

    countedFrames++;
    // wait if frame finished early
    frameTicks = capTimer.getTicks();
    if (SCREEN_TICKS_PER_FRAME > frameTicks)
      SDL_Delay(SCREEN_TICKS_PER_FRAME - frameTicks);
  }

  close();

  return 0;
}

void onGameOver(const Player* winner) {
  bool loadingResult = gTextTexture.loadFromRenderedText(
      gRenderer, gFont, (nullptr == winner) ? "Nobody won!" : winner->getName() + " won!",
      textColor);

  SDL_SetRenderDrawColor(gRenderer, 255, 255, 255, 255);
  SDL_RenderClear(gRenderer);
  gameBoard->render(gRenderer);
  SDL_RenderPresent(gRenderer);
  SDL_Delay(1000);

  while (loadingResult) {
    // Start cap time
    capTimer.start();

    if (0 != SDL_PollEvent(&gEvent)) {
      if (SDL_QUIT == gEvent.type) {
        quit = true;
      }

      if (SDL_KEYDOWN == gEvent.type) {
        switch (gEvent.key.keysym.sym) {
          case SDLK_r:
            quit = false;
            gameBoard->reset();
            break;

          case SDLK_q:
            quit = true;
            break;

          default:
            break;
        }
        break;
      }
    }

    if (quit) break;

    // Calculate and correct fps
    avgFPS = countedFrames / (fpsTimer.getTicks() / 1000.f);
    if (avgFPS > 2000000) avgFPS = 0;

#ifdef SHOW_FPS
    fpsText.str("");
    fpsText << (int) avgFPS;

    // Load fps text texture
    if (!gFPSTexture.loadFromRenderedText(gRenderer, gFont, fpsText.str().c_str(),
                                          fpsTextColor)) {
      printf("Unable to render render time text! Error: %s\n", TTF_GetError());
    }
#endif

    // Initialize renderer color
    SDL_SetRenderDrawColor(gRenderer, 255, 255, 255, 255);

    // Clear screen
    SDL_RenderClear(gRenderer);

    gTextTexture.render(
        gRenderer, (SCREEN_SIZE - gTextTexture.getWidth()) / 2,
        (SCREEN_SIZE / 2) - gTextTexture.getHeight() - (gTextTexture.getHeight() / 2));
    gReplayTexture.render(gRenderer, (SCREEN_SIZE - gReplayTexture.getWidth()) / 2,
                          (SCREEN_SIZE / 2) - (gTextTexture.getHeight() / 2));
    gQuitTexture.render(
        gRenderer, (SCREEN_SIZE - gQuitTexture.getWidth()) / 2,
        (SCREEN_SIZE / 2) + gQuitTexture.getHeight() - (gTextTexture.getHeight() / 2));

#ifdef SHOW_FPS
    gFPSTexture.render(gRenderer, 0, 0);
#endif

    // Update screen
    SDL_RenderPresent(gRenderer);

    countedFrames++;
    // wait if frame finished early
    frameTicks = capTimer.getTicks();
    if (SCREEN_TICKS_PER_FRAME > frameTicks)
      SDL_Delay(SCREEN_TICKS_PER_FRAME - frameTicks);
  }
}

bool init() {
  // Initialize SDL
  if (0 != SDL_Init(SDL_INIT_VIDEO)) {
    printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
    return false;
  }

  // Set texture filtering to linear
  if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1")) {
    printf("Warning: Linear texture filtering not enabled!");
  }

  // Create window
  gWindow =
      SDL_CreateWindow(APPLICATION_NAME, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                       SCREEN_SIZE, SCREEN_SIZE, SDL_WINDOW_SHOWN);
  if (nullptr == gWindow) {
    printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
    return false;
  }

  // Create renderer for window
  gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED);
  if (nullptr == gRenderer) {
    printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
    return false;
  }

  // initialize SDL_image
  if (0 == IMG_Init(IMG_INIT_PNG)) {
    printf("Could not initialize SDL_image!\n SDL_image Error: %s\n\n", IMG_GetError());
    return false;
  }

  // initialize SDL_ttf
  if (0 != TTF_Init()) {
    printf("Could not initialize SDL_ttf!\n SDL_ttf Error: %s\n\n", TTF_GetError());
    return false;
  }

  return true;
}

bool loadMedia() {
  gFont = TTF_OpenFont(FONTS("DotGothic16-Regular.ttf"), FONT_SIZE);
  if (nullptr == gFont) {
    printf("Could not open font in %s!\nTTF_GetError: %s\n\n",
           FONTS("DotGothic16-Regular.ttf"), TTF_GetError());
    return false;
  }

  if (!crossTexture.loadFromFile(gRenderer, TEXTURES("cross.png"))) {
    printf("Could not load texture!\nSDL_GetError: %s\n\n", SDL_GetError());
    return false;
  }

  if (!circleTexture.loadFromFile(gRenderer, TEXTURES("circle.png"))) {
    printf("Could not load texture!\nSDL_GetError: %s\n\n", SDL_GetError());
    return false;
  }

  if (!gReplayTexture.loadFromRenderedText(gRenderer, gFont, "Press R to replay",
                                           textColor)) {
    printf("Could not load texture!\nTTF_GetError: %s\n\n", TTF_GetError());
    return false;
  }

  if (!gQuitTexture.loadFromRenderedText(gRenderer, gFont, "Press Q to quit",
                                         textColor)) {
    printf("Could not load texture!\nTTF_GetError: %s\n\n", TTF_GetError());
    return false;
  }

  return true;
}

void close() {
  delete gameBoard;

  gTextTexture.free();
  gReplayTexture.free();
  crossTexture.free();
  circleTexture.free();
  gFPSTexture.free();
  gEndScreen.free();
  gQuitTexture.free();

  TTF_CloseFont(gFont);
  gFont = nullptr;

  // Destroy renderer
  SDL_DestroyRenderer(gRenderer);
  gRenderer = nullptr;

  // Destroy window
  SDL_DestroyWindow(gWindow);
  gWindow = nullptr;

  // Quit SDL subsystems
  TTF_Quit();
  IMG_Quit();
  SDL_Quit();
}
